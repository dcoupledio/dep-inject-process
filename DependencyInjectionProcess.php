<?php namespace Decoupled\Core\Bundle\Process;

use Decoupled\Core\Bundle\BundleProcess;
use Decoupled\Core\Bundle\BundleInterface;
use Decoupled\Core\Application\ApplicationContainer;

class DependencyInjectionProcess extends BundleProcess{

    public function __construct( ApplicationContainer $app )
    {
        $this->setApp( $app );
    }

    public function getName()
    {
        return 'dependency.injection.process';
    }

    public function process( BundleInterface $bundle )
    {
        $dir = $bundle->getDir();

        $name = $bundle->getName();

        $app = $this->app;

        $inflect = $app['$inflector'];

        $loader = $app['$dependency.injection.loader'];

        $loader->addPrefix( 
            $inflect( $name )->asClass() .'\\', 
            $dir 
        );

        $loader->register();
    }
}
