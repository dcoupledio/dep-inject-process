<?php namespace Decoupled\Core\Bundle\Process\Test;

use Decoupled\Core\Bundle\BundleInterface;

class AppBundle implements BundleInterface{

    /**
     * @return     string  The id of the bundle.
     */

    public function getName()
    {
        return 'app.bundle';
    }

    /**
     * @return     string  The Bundle Dir
     */

    public function getDir()
    {
        return dirname(__FILE__);
    }

}