<?php 

require '../vendor/autoload.php';

use phpunit\framework\TestCase;
use Decoupled\Core\Extension\Bundle\BundleExtension;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Core\Bundle\Process\Test\AppBundle;
use Decoupled\Core\Bundle\Process\DependencyInjectionProcess;

class ProcessTest extends TestCase{

    public function testCanAutoloadClass()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new BundleExtension() );

        $app->uses( new DependencyInjectionExtension() );

        $app->bundle()->add( new AppBundle() );

        $app->uses(function( $bundleInitializer ){

            $bundleInitializer->uses( new DependencyInjectionProcess($this) );

        });

        $app->process()->init( $app->bundle() );

        //attempt to load class from AppBundle namespace
        
        $ctrl = new \AppBundle\AppController;

        $this->assertEquals( $ctrl->test(), 1 );
    }

}

